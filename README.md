# Scatter Gather Banana Apple

Refer to exhibits. A web client submits a request to http://localhost:8081. What the structure of the payload at the end of the flow?

![alt text](image.png)

```
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core"
	xmlns:doc="http://www.mulesoft.org/schema/mule/documentation"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">
	<http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="70a07578-24d5-4434-bf1d-248992737ce6" >
		<http:listener-connection host="0.0.0.0" port="8081" />
	</http:listener-config>
	<flow name="scartter-gather-apple-bananaFlow" doc:id="3319b758-f4fb-4dfc-b92b-5ba82e4dd525" >
		<http:listener doc:name="HTTP: GET /" doc:id="d3bbec9c-393e-41b6-9fa0-efe1b3c12b1f" config-ref="HTTP_Listener_config" path="/practica"/>
		<scatter-gather doc:name="Scatter-Gather" doc:id="ccbe460d-475d-4e60-92d4-900f84583159" >
			<route >
				<flow-ref doc:name='apple' doc:id="2101f411-71da-4430-8da7-6373793518b2" name="apple"/>
			</route>
			<route >
				<flow-ref doc:name="banana" doc:id="f9fc3d87-09d8-4e50-a708-9e7d75a7cc01" name="banana"/>
			</route>
		</scatter-gather>
	</flow>
	<sub-flow name="apple" doc:id="a910ce24-691a-49c9-a418-f00b833c4e88" >
		<set-payload value='#["Apple"]' doc:name='"Apple"' doc:id="14dc2902-5a96-409b-ab29-eb683083e39f" />
	</sub-flow>
	<sub-flow name="banana" doc:id="370406e7-44f9-454e-9777-d76cd3f886f7" >
		<set-payload value='#["Banana"]' doc:name='"Banana"' doc:id="fbc508d0-4d59-4d0d-af54-f5e80a778e86" />
	</sub-flow>
</mule>

```

````
- [ ]   [ 'Banana', 'Apple']

- [ ]   {
            "0": "Banana",
            "1": "Apple"
        }
      
- [ ]   {
            "attributes": ---,
            "payload": [ 'Banana', 'Apple']
        }

- [x]   [
            "0": {
                "attributes": ---,
                "payload": "Banana"
                }
            "1": {
                "attributes": ---,
                "payload": "Apple"
                }
        ]

**Respuesta correcta:** Error - main flow